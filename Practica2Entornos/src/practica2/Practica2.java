package practica2;

import java.util.Scanner;

import metodos.Metodo1;
import metodos.Metodo2;
import metodos.Metodo3;
import metodos.Metodo4;

public class Practica2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int opcion;
		do {
		System.out.println("opcion 1:EL mayor numero de un Array de 5 aleatorio");
		System.out.println("opcion 2:EL menor numero de un Array de 5 aleatorio");
		System.out.println("opcion 3:La suma de lo numeros aleatorios de un Array de 5");
		System.out.println("opcion 4:Ordenar de menor a mayor el Array");
		System.out.println("opcion 5: Salir");
		System.out.println();
		System.out.println("Dame una opcion");
		opcion = sc.nextInt();
		switch (opcion) {
		case 1:
			Metodo1.metodo1();
			System.out.println();
			break;
		case 2:
			Metodo2.metodo2();
			System.out.println();
			break;
		case 3:
			Metodo3.metodo3();
			System.out.println();
			break;
		case 4:
			Metodo4.ordenarArray();
			System.out.println();
			break;
		case 5:
			System.out.println("Saliendo");
			break;
		default:
			System.out.println("La opcion no es valida");
			break;
		}
		} while (opcion!=5);
		sc.close();

	}

}
