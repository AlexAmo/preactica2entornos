package ventana1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.Toolkit;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Cursor;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class Ventana1 extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana1 frame = new Ventana1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana1() {
		setExtendedState(Frame.MAXIMIZED_VERT);
		setForeground(new Color(0, 153, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 495, 505);
		contentPane = new JPanel();
		contentPane.setFont(new Font("Arial", Font.PLAIN, 15));
		contentPane.setBackground(new Color(0, 128, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(new Color(0, 255, 0));
		menuBar.setBounds(0, 0, 434, 21);
		contentPane.add(menuBar);
		
		JMenu mnInicio = new JMenu("Inicio");
		menuBar.add(mnInicio);
		
		JMenuItem mntmInicio = new JMenuItem("Inicio");
		mnInicio.add(mntmInicio);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Cerrar sesion");
		mnInicio.add(mntmNewMenuItem);
		
		JMenu mnNewMenu_1 = new JMenu("Suscripcion");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNormal = new JMenuItem("Normal");
		mnNewMenu_1.add(mntmNormal);
		
		JMenuItem mntmPremium = new JMenuItem("Premium");
		mnNewMenu_1.add(mntmPremium);
		
		JMenu mnNewMenu_2 = new JMenu("Mesas");
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmReservarMesa = new JMenuItem("Reservar mesa");
		mnNewMenu_2.add(mntmReservarMesa);
		
		JMenuItem mntmVerMapa = new JMenuItem("Ver mapa");
		mnNewMenu_2.add(mntmVerMapa);
		
		JMenu mnNewMenu_3 = new JMenu("Tienda");
		menuBar.add(mnNewMenu_3);
		
		JMenuItem mntmReservarJuego = new JMenuItem("Reservar juego");
		mnNewMenu_3.add(mntmReservarJuego);
		
		JMenuItem mntmComprarJuego = new JMenuItem("Comprar juego");
		mnNewMenu_3.add(mntmComprarJuego);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Ventana1.class.getResource("/images/download.jpg")));
		label.setBounds(10, 121, 224, 233);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Ventana1.class.getResource("/images/downloa.jpg")));
		label_1.setBounds(244, 143, 224, 225);
		contentPane.add(label_1);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("A\u00F1adir al carro");
		chckbxNewCheckBox.setForeground(new Color(0, 0, 0));
		chckbxNewCheckBox.setBackground(new Color(0, 255, 0));
		chckbxNewCheckBox.setBounds(10, 407, 142, 23);
		contentPane.add(chckbxNewCheckBox);
		
		JCheckBox chckbxAadirAlCarro = new JCheckBox("A\u00F1adir al carro");
		chckbxAadirAlCarro.setSelected(true);
		chckbxAadirAlCarro.setForeground(new Color(0, 0, 0));
		chckbxAadirAlCarro.setBackground(new Color(0, 255, 0));
		chckbxAadirAlCarro.setBounds(244, 407, 136, 23);
		contentPane.add(chckbxAadirAlCarro);
		
		JLabel lblNewLabel = new JLabel("Mysterium");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setBackground(new Color(0, 255, 0));
		lblNewLabel.setBounds(244, 379, 116, 21);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Expansion Catan :Cities & Knigths");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setForeground(new Color(0, 0, 0));
		lblNewLabel_1.setBackground(new Color(0, 255, 0));
		lblNewLabel_1.setBounds(10, 379, 224, 21);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("20,0\u20AC");
		lblNewLabel_2.setForeground(new Color(0, 0, 0));
		lblNewLabel_2.setBackground(new Color(0, 255, 0));
		lblNewLabel_2.setBounds(157, 408, 46, 21);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("39,99\u20AC");
		lblNewLabel_3.setForeground(new Color(0, 0, 0));
		lblNewLabel_3.setBackground(new Color(0, 255, 0));
		lblNewLabel_3.setBounds(386, 411, 63, 14);
		contentPane.add(lblNewLabel_3);
		
		JButton btnNewButton = new JButton("CARRO");
		btnNewButton.setBackground(new Color(0, 255, 0));
		btnNewButton.setBounds(21, 76, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel_4 = new JLabel("39,99");
		lblNewLabel_4.setForeground(new Color(0, 255, 0));
		lblNewLabel_4.setBackground(new Color(0, 255, 0));
		lblNewLabel_4.setBounds(120, 76, 53, 23);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("TIENDA");
		lblNewLabel_5.setVerifyInputWhenFocusTarget(false);
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_5.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel_5.setForeground(new Color(0, 0, 0));
		lblNewLabel_5.setBackground(new Color(0, 255, 0));
		lblNewLabel_5.setBounds(10, 21, 458, 42);
		contentPane.add(lblNewLabel_5);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setForeground(new Color(0, 255, 0));
		comboBox.setBackground(new Color(0, 128, 0));
		comboBox.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		comboBox.setToolTipText("");
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Ni\u00F1os", "Larga duracion", "Corta duracion", "Con figuras"}));
		comboBox.setName("Categorias");
		comboBox.setBounds(313, 77, 136, 20);
		contentPane.add(comboBox);
		
		JButton btnBuscar = new JButton("BUSCAR");
		btnBuscar.setBackground(new Color(0, 255, 0));
		btnBuscar.setBounds(198, 74, 89, 23);
		contentPane.add(btnBuscar);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Reservar");
		rdbtnNewRadioButton.setBackground(new Color(0, 255, 0));
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(10, 436, 109, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Reservar");
		rdbtnNewRadioButton_1.setBackground(new Color(0, 255, 0));
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setBounds(244, 436, 109, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JLabel lblNewLabel_6 = new JLabel("AVISO:Solo puedes reservar un juego al mismo tiempo");
		lblNewLabel_6.setForeground(new Color(255, 165, 0));
		lblNewLabel_6.setBackground(new Color(255, 255, 255));
		lblNewLabel_6.setBounds(20, 61, 414, 14);
		contentPane.add(lblNewLabel_6);
	}
}
